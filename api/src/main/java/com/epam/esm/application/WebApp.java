package com.epam.esm.application;

import com.epam.esm.config.JpaConfig;
import com.epam.esm.config.MapperConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication(scanBasePackages = "com.epam.esm")
@Import({JpaConfig.class, MapperConfig.class})
public class WebApp {

    public static void main(String[] args) {
        SpringApplication.run(WebApp.class, args);
    }
}