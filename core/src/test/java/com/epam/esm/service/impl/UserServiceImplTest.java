package com.epam.esm.service.impl;

import com.epam.esm.config.MapperConfig;
import com.epam.esm.config.TestConfig;
import com.epam.esm.model.Tag;
import com.epam.esm.model.User;
import com.epam.esm.repository.UserRepository;
import com.epam.esm.repository.exception.UserOperationNotSupportedException;
import com.epam.esm.service.UserService;
import com.epam.esm.service.dto.TagDTO;
import com.epam.esm.service.dto.UserDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@Profile("test")
@SpringBootTest(classes = {TestConfig.class, MapperConfig.class})
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Autowired
    private ModelMapper modelMapper;

    private UserService userService;

    private User testUser;

    @BeforeEach
    void setUp() {
        userService = new UserServiceImpl(userRepository, modelMapper);
        testUser = new User();
        testUser.setName("Test User name");
        testUser.setEmail("email@dot.com");
    }

    @Test
    void getValidUser_success_test() {
        String actual = "Test User name";

        when(userRepository.findById(anyLong())).thenReturn(Optional.of(testUser));
        assertEquals(userService.get(1L).getName(), actual);
        verify(userRepository).findById(1L);
    }

    @Test
    void getAllUsers_test() {

        List<User> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User user = new User();
            user.setName("User#" + i);
            user.setEmail("mail" + i + "@dot.com");
            list.add(user);
        }

        when(userRepository.findAll()).thenReturn(list);

        List<UserDTO> all = userService.getAll();

        verify(userRepository).findAll();
        assertEquals(5, all.size());
    }

    @Test
    void checkUserExistsById() {
        testUser.setId(1L);
        when(userRepository.existsById(anyLong())).thenReturn(true);
        boolean exists = userService.isExists(1L);
        assertTrue(exists);
        verify(userRepository).existsById(anyLong());
    }

    @Test
    void findWithPagination_test() {

        int page = 0;
        int size = 5;

        List<User> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            User user = new User();
            user.setName("User#" + i);
            user.setEmail("mail" + i + "@dot.com");
            list.add(user);
        }

        Page<User> pageResult = new PageImpl<>(list);

        when(userRepository.findAll(any(Pageable.class)))
                .thenReturn(pageResult);

        userService.findWithPagination(page, size);

        verify(userRepository, times(1))
                .findAll(any(Pageable.class));

        Assertions.assertEquals(5, list.size());
    }

    @Test
    void getExceptions_test() {
        UserDTO mapped = modelMapper.map(testUser, UserDTO.class);
        assertAll(
                () -> assertThrows(UserOperationNotSupportedException.class,
                        () -> userService.add(mapped)),
                () -> assertThrows(UserOperationNotSupportedException.class,
                        () -> userService.update(mapped)),
                () -> assertThrows(UserOperationNotSupportedException.class,
                        () -> userService.delete(1L))
        );
    }

}