package com.epam.esm.model;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"name"}, callSuper = false)
@ToString()
@Slf4j
@Entity
@Table(name = "tag")
public class Tag extends BaseEntity {

    @Column(name = "name")
    private String name;

    @ManyToMany(mappedBy = "tags")
    private Set<Certificate> certificates;

    public Tag(Long id, String name) {
        super(id);
        this.name = name;
    }

    @PrePersist
    public void onPrePersist() {
        log.info("{} added", this);
    }

    @PreRemove
    public void onPreRemove() {
        log.info("{} removed", this);
    }
}
