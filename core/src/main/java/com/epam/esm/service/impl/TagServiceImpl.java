package com.epam.esm.service.impl;

import com.epam.esm.model.Tag;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.repository.dao.TagDAO;
import com.epam.esm.repository.exception.TagFailCreateException;
import com.epam.esm.repository.exception.TagNotFoundException;
import com.epam.esm.service.TagService;
import com.epam.esm.service.dto.TagDTO;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional(readOnly = true)
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;
    private final TagDAO tagDAO;
    private final ModelMapper modelMapper;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository, TagDAO tagDAO, ModelMapper modelMapper) {
        this.tagRepository = tagRepository;
        this.tagDAO = tagDAO;
        this.modelMapper = modelMapper;
    }

    @Override
    @Transactional
    public TagDTO add(TagDTO tagDTO) {
        if (isExists(tagDTO.getName())) {
            throw new TagFailCreateException("The name '" + tagDTO.getName() + "' is already in use. Please choose a different name");
        }
        Tag tag = modelMapper.map(tagDTO, Tag.class);
        Tag saved = tagRepository.save(tag);
        return modelMapper.map(saved, TagDTO.class);
    }

    @Override
    public TagDTO update(TagDTO tagDTO) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Transactional
    public boolean delete(Long id) {
        try {
            tagRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public TagDTO get(Long id) {
        Optional<Tag> tagOptional = tagRepository.findById(id);
        if (tagOptional.isEmpty()) throw new TagNotFoundException();
        return modelMapper.map(tagOptional, TagDTO.class);
    }

    @Override
    public List<TagDTO> getAll() {
        return tagRepository.findAll().stream()
                .map(tag -> modelMapper.map(tag, TagDTO.class))
                .toList();
    }

    @Override
    public boolean isExists(Long id) {
        return tagRepository.existsById(id);
    }

    @Override
    public boolean isExists(String name) {
        return tagRepository.findByName(name).isPresent();
    }

    @Override
    public List<TagDTO> findWithPagination(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Tag> tags = tagRepository.findAll(pageable);
        return tags.getContent().stream()
                .map(t -> modelMapper.map(t, TagDTO.class))
                .toList();
    }

    @Override
    public TagDTO getWidelyUsedTag(Long userId) {
        Long tagId = tagDAO.getWidelyUsedTag(userId);
        if (tagId == null) throw new TagNotFoundException();
        return get(tagId);
    }
}