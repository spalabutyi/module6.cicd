package com.epam.esm.service;


import com.epam.esm.service.dto.UserDTO;

import java.util.List;

public interface UserService extends Service<UserDTO> {

    /**
     * Retrieves a list of UserDTO objects with pagination.
     *
     * @param page the page number of the result set to retrieve
     * @param size the maximum number of results to retrieve per page
     * @return a List of UserDTO objects that match the specified criteria
     */
    List<UserDTO> findWithPagination(Integer page, Integer size);

}