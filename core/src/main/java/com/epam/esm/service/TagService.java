package com.epam.esm.service;

import com.epam.esm.service.dto.TagDTO;

import java.util.List;

public interface TagService extends Service<TagDTO> {

    /**
     * Returns a list of TagDTO objects with pagination.
     *
     * @param page the page number
     * @param size the number of tags per page
     * @return a list of TagDTO objects
     */
    List<TagDTO> findWithPagination(Integer page, Integer size);

    /**
     * Returns the TagDTO object that is widely used by the user with the specified user ID.
     *
     * @param userId the ID of the user
     * @return the TagDTO object
     */
    TagDTO getWidelyUsedTag(Long userId);

}
