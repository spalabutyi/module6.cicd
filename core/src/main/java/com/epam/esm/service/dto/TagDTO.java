package com.epam.esm.service.dto;

import lombok.*;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"name"}, callSuper = false)
@ToString
@Component
public class TagDTO extends AbstractDTO {

    @NotBlank(message = "Name is required")
    @Size(min = 1, max = 16, message = "Name must be between 1 and 16 characters")
    private String name;

    public TagDTO(String name) {
        this.name = name;
    }

    public TagDTO(Long id, String name) {
        super(id);
        this.name = name;
    }

}