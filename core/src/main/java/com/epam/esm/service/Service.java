package com.epam.esm.service;

import com.epam.esm.service.dto.AbstractDTO;

import java.util.List;

/**
 * Service interface for managing entities that extend AbstractDTO.
 */
public interface Service<T extends AbstractDTO> {

    /**
     * Adds a new entity to the system.
     *
     * @param t the entity to add.
     * @return true if the operation was successful, false otherwise.
     */
    T add(T t);

    /**
     * Updates an existing entity in the system.
     *
     * @param t the entity to update.
     * @return true if the operation was successful, false otherwise.
     */
    T update(T t);

    /**
     * Deletes an entity from the system.
     *
     * @param id the id of the entity to delete.
     * @return true if the operation was successful, false otherwise.
     */
    boolean delete(Long id);

    /**
     * Retrieves an entity from the system.
     *
     * @param id the id of the entity to retrieve.
     * @return the entity with the given id, or null if it does not exist.
     */
    T get(Long id);

    /**
     * Retrieves a list of all entities in the system.
     *
     * @return a list of all entities.
     */
    List<T> getAll();

    /**
     * Checks if an entity with the given id exists in the system.
     *
     * @param id the id to check.
     * @return true if an entity with the given id exists, false otherwise.
     */
    boolean isExists(Long id);

    /**
     * Checks if an entity with the given name exists in the system.
     *
     * @param name the name to check.
     * @return true if an entity with the given name exists, false otherwise.
     */
    boolean isExists(String name);
}