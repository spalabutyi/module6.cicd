package com.epam.esm.repository.dao.impl;

import com.epam.esm.model.Tag;
import com.epam.esm.repository.dao.AbstractDAO;
import com.epam.esm.repository.dao.TagDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class TagDAOImpl extends AbstractDAO implements TagDAO {

    @Autowired
    public TagDAOImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Long getWidelyUsedTag(Long id) {
        List<Long> list = jdbcTemplate.queryForList(Query.GET_WIDELY_USED_TAG, new Object[]{id}, Long.class);
        return list.isEmpty() ? null : list.get(0);
    }

    @Override
    public Tag add(Tag tag) {
        return null;
    }

    @Override
    public boolean update(Tag tag) {
        return false;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    @Override
    public Tag get(Long id) {
        return null;
    }

    @Override
    public List<Tag> getAll() {
        return Collections.emptyList();
    }

    @Override
    public boolean isExists(Long id) {
        return false;
    }

    @Override
    public boolean isExists(String name) {
        return false;
    }
}
