package com.epam.esm.repository.dao;

import com.epam.esm.model.Tag;
import org.springframework.stereotype.Component;

@Component
public interface TagDAO extends Dao<Tag> {

    /**
     * Returns the ID of the most widely used tag for the given User ID.
     *
     * @param id the ID of the tag to search for
     * @return the ID of the most widely used tag for the given User ID
     */
    Long getWidelyUsedTag(Long id);

}
