package com.epam.esm.repository.exception;

public class CertificateAddTagException extends RuntimeException{

    public CertificateAddTagException(String message) {
        super(message);
    }

}
